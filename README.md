# Mega uBlackList List

## 一个超级巨大量的黑名单, 近 5 万 5 千条屏蔽网站

### 适用于 _uBlackList_ 浏览器插件

#### 因黑名单过于宽泛, 切随着时间推移可能里面有部分网站会变成正常内容网站, 导致任何你想访问的网站被屏蔽 _请自行对此表增删改查_

订阅链接 👇

> [GitHub 直链][3]
>
> [大陆友好][4]
>
> [大的来了][5] //编辑: 不推荐直接订阅, 因为这玩意儿超大的, 每次想要添加新的屏蔽网站插件要加载十万年
>
> [大的国内友好][6]

同时推荐订阅下面链接的内容

> https://raw.githubusercontent.com/eallion/uBlacklist-subscription-compilation/main/uBlacklist.txt

> https://github.com/cobaltdisco/Google-Chinese-Results-Blocklist/blob/master/uBlacklist_subscription.txt

# Universal Web Filter rules

## 个人采集的网页元素屏蔽规则

### 适用于使用 _AdBlock 语法_ 的浏览器插件

直接在设置->My Filter 中导入文件 [My Filter.txt][1] 或 [大陆友好链接][2] 即可

[1]: https://github.com/isNijikawa/Universal-Web-Filter-rules/blob/main/uBlock%20Origin/My%20Filter.txt
[2]: https://cdn.jsdelivr.net/gh/isNijikawa/Universal-Web-Filter-rules@main/uBlock%20Origin/My%20Filter.txt
[3]: https://github.com/isNijikawa/Universal-Web-Filter-rules/blob/main/uBlack%20List/stupidCyberUtopia.txt
[4]: https://cdn.jsdelivr.net/gh/isNijikawa/Universal-Web-Filter-rules@main/uBlack%20List/stupidCyberUtopia.txt
[5]: https://github.com/isNijikawa/Universal-Web-Filter-rules/blob/main/uBlack%20List/yetAnother.txt
[6]: https://cdn.jsdelivr.net/gh/isNijikawa/Universal-Web-Filter-rules@main/uBlack%20List/yetAnother.txt
